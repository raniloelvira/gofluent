package com.shoppingcart.service;

import java.util.List;

import com.shoppingcart.dto.ProductDto;

public interface ProductService {
	
	public List<ProductDto> getProductList();

}