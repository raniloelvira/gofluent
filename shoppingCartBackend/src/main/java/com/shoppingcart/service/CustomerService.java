package com.shoppingcart.service;

import java.util.List;

import com.shoppingcart.dto.CustomerDto;

public interface CustomerService {
	
	public List<CustomerDto> getCustomerList();
	void saveCustomerData(CustomerDto customer);

}