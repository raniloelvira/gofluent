package com.shoppingcart.dto;

public class ProductDto implements java.io.Serializable {

	private int productId;
	private String productDesc;
	private Double price;

	public ProductDto() {
	}

	public ProductDto(int productId) {
		this.productId = productId;
	}

	public ProductDto(int productId, String productDesc, Double price) {
		this.productId = productId;
		this.productDesc = productDesc;
		this.price = price;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
