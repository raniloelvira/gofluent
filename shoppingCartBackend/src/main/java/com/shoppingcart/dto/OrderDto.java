package com.shoppingcart.dto;

public class OrderDto implements java.io.Serializable {

	private int orderId;
	private int customerId;
	private Integer productId;
	private Double price;
	private Integer quantity;
	private Double totalPrice;

	public OrderDto() {
	}

	public OrderDto(int orderId) {
		this.orderId = orderId;
	}

	public OrderDto(int orderId, int customerId, Integer productId, Double price, Integer quantity, Double totalPrice) {
		super();
		this.orderId = orderId;
		this.customerId = customerId;
		this.productId = productId;
		this.price = price;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

}
