import { Injectable } from '@angular/core';

@Injectable()
export class AppGlobals {
    readonly fullTitle: string = 'Shopping Cart'; //Smart
    readonly shortTitle: string = 'shoppingCart'; //SCS
}

@Injectable()
export class GlobalConfig {
    //Table - primeng
	readonly responsive: boolean = true;
    readonly paginator: boolean = true;
    readonly rows: number = 10;
    readonly paginatorPosition: string = 'top';
    readonly resizableColumns: boolean = true;
    readonly reorderableColumns: boolean = true;
    readonly columnResizeMode: string = 'fit';
    readonly scrollable: boolean = true;
    readonly rowsPerPageOptions: Array<any> = [10,20,30];
    readonly compareSelectionBy: string = 'equals';
    readonly tableNonEdit: string = 'col-md-12 non-editable';
    readonly tableEdit: string = 'col-md-12';
    //css class
    readonly panel_header: string = 'card-header header-padding';
    readonly panel_header_button: string = 'btn btn-primary btn-color btn-text-bold';
    readonly panel_buttons_position: string = 'div-buttons';
    //Message
    readonly messageLife: number = 2000;
    //Date format
    readonly dateFormat: string = 'dd-MM-yyyy HH:mm';
    readonly dateTimeFormat: string = 'dd-MM-yyyy HH:mm:ss';
    readonly dateOnlyFormat: string = 'dd-MM-yyyy';
    readonly timeOnlyFormat: string = 'HH:mm';
}
