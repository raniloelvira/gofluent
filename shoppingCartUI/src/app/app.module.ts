import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppConfigModule } from './app.config';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import { NoContentComponent } from './views/no-content/no-content.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared.module';
import { HeaderComponent } from './views/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { CustomerComponent } from './views/customer/customer.component';
import { CustomerService } from './service/customer.service';
import { OrderComponent } from './views/order/order.component';
import { OrderService } from './service/order.service';
import { ProductService } from './service/product.service';

@NgModule({
  declarations: [
    AppComponent,
    NoContentComponent,
    HeaderComponent,
    CustomerComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    //AppRoutingModule,
    AppConfigModule,
    NgbModule,
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    BrowserAnimationsModule,
    FormsModule,
    SharedModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    CustomerService,
    OrderService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
