import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from '../app.config';
import { Customer } from '../model/customer.model';
import { CommitStatus } from '../model/commit-status.model';

@Injectable()
export class CustomerService {

    constructor(
        private httpClient: HttpClient,
        @Inject(APP_CONFIG) private config: AppConfig
    ) {}
    
    public loadData() {
        return this.httpClient
        .get<Array<Customer>>(this.config.apiEndpoint +'/customer/getCustomerList')
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

    public saveCustomerData(customer: Customer) {
        return this.httpClient
        .post<CommitStatus>(this.config.apiEndpoint +'/customer/saveCustomerData', 
            customer)
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

}