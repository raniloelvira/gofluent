export class Product {
    constructor(
        public productId?: number,
        public productDesc?: string,
        public price?: number){
    }
}